<?php
  //Start session
  session_start();
  //Initialize sessions
  $sessionid = $_SESSION['login_session'];
  //Destroy sessions
  if(session_destroy()){
    //Require database
    require 'db.inc.php';
    //Remove data from database
    $DB->query("UPDATE users SET session_id=null, admin_id=null WHERE session_id='$sessionid'");
    //if session is destroyed send back to index
    header('Location: ./index.php');
    die();
  }
  else{
    //An error has occured while loggin out.
    header('Location: ./index.php?e='.base64_encode("An error occured while logging out."));
    die();
  }

?>
