<?php
  /*
    Aad van Esschoten
  */
  //define site variables
  define('SITE_TITLE', 'TicketStore');
  define('SITE_LOGO', 'TicketStore');
  //start sessie
  session_start();
  //Check if you are on the login page to remove the login button
  $loginPage = false;
  //Require database file
  require 'db.inc.php';
  require 'loadClasses.php';
  //Define when logged in
  if(isset($_SESSION['login_session'])){
    $login_session = $_SESSION['login_session'];
    $email = $_SESSION['email'];
    //Require Database
    $sql = "SELECT * FROM users WHERE session_id='$login_session' AND email='$email'";
    $result = $DB->query($sql);
    if($result->num_rows > 0){
      $rows = $result->fetch_assoc();
      $name = $rows['name'];
      $user_id = $rows['id'];
      define('USER_NAME', $name);
    }
    else{
      header('Location: ./logout.php');
    }
  }
  $i = 0;
?>
