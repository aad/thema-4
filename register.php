<?php require 'config.inc.php'; //Include the config?>
<?php $loginPage = true; ?>
<?php require 'header.page.php'; //Include the page header?>
<div class="wrapper">
  <div class="loginForm">
    <h1>Register</h1>
    <?php if(isset($_GET['e'])):?>
      <div class="alert alert-danger" role="alert"><?php print base64_decode($_GET['e']);?></div>
    <?php endif;?>
    <form action="reg_auth.php" method="post">
      <table>
        <tr>
          <td>Name:</td>
          <td><input type="text" name="userName" placeholder="Name" required></td>
        </tr>
        <tr>
          <td>Email:</td>
          <td><input type="email" name="email" placeholder="Email" required></td>
        </tr>
        <tr>
          <td>Password:&nbsp;&nbsp;&nbsp;</td>
          <td><input type="password" name="password" placeholder="Password" required></td>
        </tr>
        <tr>
          <td>Confirm Password:&nbsp;&nbsp;&nbsp;</td>
          <td><input type="password" name="confirmPassword" placeholder="Confirm Password" required></td>
        </tr>
        <tr>
          <td>&nbsp;&nbsp;&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
          <td><a href="login.php">Login</a></td>
          <td><input type="submit" name="submitBtn" class="btn btn-primary" value="Register"></td>
        </tr>
      </table>
    </form>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
