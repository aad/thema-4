<?php require 'check.admin.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<div class="wrapper">
  <?php require 'sidebar.page.php'; //Include the admin sidebar?>
  <div class="right-admin-side">
    <a href="create-product.php" class="btn btn-primary" style="float: right;">New Product</a><br />
    <?php
      $sql = "SELECT * FROM products";
      $result = $DB->query($sql);
      if($result->num_rows > 0):
    ?>
    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Available</th>
          <th>Quantity</th>
          <th>Price</th>
          <th></th>
        </tr>
      </thead>
        <tbody>
    <?php
        while($row = $result->fetch_assoc()):
          $PRid = $row['id'];
          $PRName = $row['name'];
          $PRPrice = $row['price'];
          $PRQuantity = $row['quantity'];
          $PRAvailable = $row['available'];
        ?>
        <tr>
        <td><?php print $PRName; ?></td>
        <td><?php print $PRAvailable; ?></td>
        <td><?php print $PRQuantity; ?></td>
        <td><?php print $PRPrice; ?></td>
        <td><a href="change_product.php?id=<?php print $PRid;?>" class="btn btn-primary"><i class="fa fa-cog"></i></a></td>
        </tr>
        <?php
        endwhile;
        ?>
      </tbody>
    </table>
        <?php
      else:
        print "No products yet.";
      endif;
    ?>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
