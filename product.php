<?php require 'config.inc.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<?php
  if(!isset($_GET['id'])){
    header('Location: ./tickets.php');
    die();
  }
  else{
    $productId = strip_tags(htmlspecialchars($_GET['id']));
    $sql = "SELECT * FROM products WHERE id='$productId'";
    $result = $DB->query($sql);
    if($result->num_rows < 1){
      print "<script>window.location.href = 'tickets.php';</script>";
      die();
    }

    //Fetch product data
    $data = $result->fetch_assoc();
    $imageSrc = $data['image'];
    $title = $data['name'];
    $price = $data['price'];
    $description = $data['description'];
  }
?>
<div class="wrapper">
  <a href="./tickets.php" class="btn btn-primary btn-outline btn-sx">Back</a><br />
  <div style="margin-bottom: 20px;"></div>
  <div>
    <div>
      <img src="<?php print $imageSrc;?>" alt="<?php print $title;?>" width="500px;" height="350px;"/>
    </div>
    <div style="margin-left: 510px; margin-top: -350px; font-size: 35px;">
      <?php print $title; ?>
    </div>
    <form action="#" method="post" style="float: right; margin-top: -35px;">
      <input type="hidden" name="product_id" value="<?php print $productId;?>">
      <input type="number" name="amount" value="1" style="width: 50px; height: 30px; border-radius: 10px; text-align: right; border: 1px solid black;" required>
      <input type="submit" name="addToCard" class="btn btn-success btn-outline btn-sx" value="Add to Cart">
    </form>
    <div style="margin-left: 510px; margin-top: 10px; font-size: 25px; color: #A8A8A8;">
        &euro;<?php print $price; ?>
    </div>
    <div style="margin-left: 510px; margin-top: 10px; margin-bottom: 350px; font-size: 15px;">
        <?php print $description; ?>
    </div>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
