<?php
  //1
  //Check if submit button is pressed.
  if (isset($_POST['submitBtn'])) {
    //Set post values into variables and sanitize them.
    $username = strip_tags(htmlspecialchars($_POST['userName']));
    $email = strip_tags(htmlspecialchars($_POST['email']));
    $password = sha1(strip_tags(htmlspecialchars($_POST['password'])));
    $cPassword = sha1(strip_tags(htmlspecialchars($_POST['confirmPassword'])));
    //2
    //Check if everything is filled in.
    if(isset($username) && isset($email) && isset($password) && isset($cPassword)){
      //3
      //Check if the passwords match
      if($password == $cPassword){
        //Require the database
        require 'db.inc.php';
        //Check if this user already exists.
        //Prepare sql
        $sql = "SELECT * FROM users WHERE email='$email'";
        //Run the query
        $result = $DB->query($sql);
        //4
        //Check if there are no records with this email adres.
        if($result->num_rows == 0){
          //Prepare insert sql
          $sql = "INSERT INTO users (name, email, password) VALUES ('$username','$email','$password')";
          //Run the query
          $result = $DB->query($sql);
          //5
          //Check if the insert was done correctly
          if($result){
            //Start the login part
            //Date time
            $date = date('Y-m-d G:i:s');

            //Start session
            session_start();
            $_SESSION['login_session'] = sha1(rand(40, 999).$date.$email.rand(10,500));
            $_SESSION['email'] = $email;
            //Update database with session id
            $sessionId = $_SESSION['login_session'];
            $DB->query("UPDATE users SET session_id='$sessionId'");
            //Header to the index logged in.
            header('Location: ./tickets.php');
            die();
          }
          //5
          else{
            die("An error has occured while adding to the database.<br /> Error: 5");
          }
        }
        //4
        else{
          header('Location: ./register.php?e='.base64_encode("This email has already been taken."));
          die();
        }
      }
      //3
      else{
        header('Location: ./register.php?e='.base64_encode("The passwords did not match."));
        die();
      }
    }
    //2
    else{
      header('Location: ./register.php?e='.base64_encode("You need to fill in all the fields."));
      die();
    }
  }
  //1
  else{
    header('Location: ./register.php');
    die();
  }
?>
