<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php print SITE_TITLE; ?></title>
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" href="css/main.css" media="screen">
    <link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/uploadForm.css">
    <link rel="stylesheet" href="css/shopping-cart.css">
    <link rel="stylesheet" href="css/credit-card.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <link rel="stylesheet" href="css/normalize.min.css">
	  <link rel="stylesheet" href="css/slider.css">
	  <link rel='stylesheet prefetch' href='css/animate.min.css'>
  </head>
  <body>
    <div class="header">
      <div id="logo"><a href="tickets.php"><?php print SITE_LOGO; ?></a></div>
      <div id="searchBar">
        <form action="tickets.php" method="get">
          <input type="search" name="q" id="q" placeholder="Search">
        </form>
      </div>
      <?php
        //Check if add to card is pressed.
        if(isset($_POST['addToCard'])){
          //Check if login session is set
          $sessionId = $_SESSION['login_session'];
          if(isset($sessionId)){
            //Set variables of inputs
            $product_id = strip_tags(htmlspecialchars($_POST['product_id']));
            $amount = strip_tags(htmlspecialchars($_POST['amount']));
            //Check if both fields are filled in.
            if($product_id && $amount){
              //Check if there are enough tickets Available
              $result = $DB->query("SELECT * FROM products WHERE id='$product_id'");
              $data = $result->fetch_assoc();
              $available = $data['available'];

              if($available >= $amount){
              //Check if the amount is an round number
              if(ctype_digit($amount)){
                //Check if the product exists in the database
                $sql = "SELECT * FROM products WHERE id='$product_id'";
                $result = $DB->query($sql);
                if($result->num_rows > 0){
                  $date_added = date('Y-m-d G:i:s');
                  $sql = "INSERT INTO cart (product_id, amount, user_id, date_added) VALUES ('$product_id','$amount','$user_id', '$date_added')";
                  $result = $DB->query($sql);
                  if($result){
                    print "<script>
                              bootbox.confirm('Do you want to go to your cart to check out? Cancel if you want to show further.', function(result) {
                                  if(result == true){
                                    window.location.replace('cart.php');
                                  }
                                });
                          </script>";
                  }
                  else {
                    print "<script>
                              bootbox.alert('An error has occured while adding this product to your cart.');
                          </script>";
                  }
                }
                else{
                  print "<script>
                            bootbox.alert('This product was not found in our database.');
                        </script>";
                }
              }
              else{
                print "<script>
                          bootbox.alert('The amount needs to be a round number.');
                      </script>";
              }
            }
            else{
              print "<script>
                        bootbox.alert('There are not enough tickets available.');
                    </script>";
            }
            }
            else{
              print "<script>
                        bootbox.alert('You need to fill in an amount.');
                    </script>";
            }
          }
          else{
            print "<script>
                      bootbox.confirm('You need to be logged in to order tickets.<br /> Do you want to continue to the login screen?', function(result) {
                          if(result == true){
                            window.location.replace('login.php');
                          }
                        });
                  </script>";
          }
        }
      ?>
      <div id="cartButton">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <?php
          $sql = "SELECT cart.amount , products.price , products.name, products.image, products.price
                  FROM cart
                  LEFT JOIN products
                  ON cart.product_id=products.id WHERE cart.user_id = '$user_id'";
          $result = $DB->query($sql);?>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-shopping-cart"></span> <?php print $result->num_rows;?> - Items<span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-cart" role="menu">
            <?php
            if($result->num_rows > 0):
              $sql = "SELECT cart.amount , products.price , products.name, products.image, products.price
                      FROM cart
                      LEFT JOIN products
                      ON cart.product_id=products.id WHERE cart.user_id = '$user_id' ORDER BY cart.date_added LIMIT 5";
              $result = $DB->query($sql);
              $totalCardPrice = 0;
              $CPC = new createProduct();
                while($rows = $result->fetch_assoc()):
                  $cartAmount = $rows['amount'];
                  $cartPrice = $rows['price'];
                  $cartImage = $rows['image'];
                  $cartName = $rows['name'];
                  $cartTotalPrice = $cartAmount*$cartPrice;
                  $totalCardPrice = $totalCardPrice+$cartTotalPrice;
            ?>
              <li>
                  <span class="item">
                    <span class="item-left">
                        <img src="<?php print $cartImage;?>" alt="<?php print $cartName;?>" width="50px"/>
                        <span class="item-info">
                            <span><?php print $cartName;?></span>
                            <span>Quantity: <?php print $cartAmount;?></span>
                            <span>Total: &euro; <?php print $cartTotalPrice;?></span>
                        </span>
                    </span>
                </span>
              </li>
            <?php endwhile; ?>
            <li style="padding-left: 10px;">
              <hr />
              Total: &euro; <?php print $totalCardPrice;?> excl.<br />
              <hr />
            </li>
            <?php else: ?>
              <li style="padding-left: 10px;">
                    There are no products<br />
                    in your cart.
              </li>
            <?php endif;?>
            <li>
                <span class="item">
                    <a href="cart.php"><center>Show Cart</center></a>
                </span>
            </li>
      </ul>
    </div>
      <div id="loginStatus">
        <?php if(isset($_SESSION['login_session'])):?>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php print USER_NAME;?>&nbsp; <span class="caret"></a>
              <ul class="dropdown-menu dropdown-cart" role="menu">
                  <?php if(isset($_SESSION['admin_session'])): ?>
                  <li>
                    &nbsp;&nbsp;Admin:
                  </li>
                  <li>
                      <span class="item">
                        <a href="costumers.php">Admin Panel</a>
                      </span>
                  </li>
                  <hr />
                  <?php endif; ?>
                  <li>
                      <span class="item">
                        <a href="my-orders.php">My Orders</a>
                      </span>
                  </li>
                  <li>
                      <span class="item">
                        <a href="my-tickets.php">My Tickets</a>
                      </span>
                  </li>
                  <hr />
                  <li>
                      <span class="item">
                        <a href="logout.php">Log Out</a>
                      </span>
                  </li>

              </ul>
            </li>
          </ul>
        <?php elseif($loginPage == true):?>
          &nbsp;
        <?php else:?>
          <div id="spacer1"></div>
          <a href="login.php">Login</a>
        <?php endif;?>
      </div>
    </div>
