<?php
  require 'check.admin.php';
  //Check if submit buttin is set
  if (isset($_POST['submitBtn'])) {
    //Set Inputs into variables
    $name = strip_tags(htmlspecialchars($_POST['name']));
    $price = strip_tags(htmlspecialchars($_POST['price']));
    $description = strip_tags(htmlspecialchars($_POST['description']));
    $Evtdate = strip_tags(htmlspecialchars($_POST['date']));
    $quantity = strip_tags(htmlspecialchars($_POST['quantity']));
    $image = $_FILES['images'];
    //Check if all fields are filled in
    if($name && $price && $description && $quantity && $image && $Evtdate){
      //Get Classes and database
      require 'loadClasses.php';
      require 'db.inc.php';

      //Set the date of today
      $TodayDate = date('Y-m-d G:i:s');

      //Set Create Product Class
      $CPC = new createProduct();
      $id = $CPC->makeId();

      if(strpos($price, '.') || strpos($price, ',')){
        //if number is comma then change it to a dot
        $price = $CPC->commaCheck($price);
      }

      //Check if price is numeric
      if(is_numeric($price)){
        //make the price always a 2 number decimal
        $price = $CPC->setDecimals($price);
        //Check if quantity is numeric
        if(is_numeric($quantity)){
          //Check if the name is under the 50 characters
          if(strlen($name) < 51){
            //Get file propertys
            $imageName = $image['name'];
            $imageTmp = $image['tmp_name'];
            $imageExt = strtolower(end(explode('.', $imageName)));
            //Check if file is an image
            $allowedExt = array('jpeg','jpg','png');
            if(in_array($imageExt ,$allowedExt)){
              //Check if uploads dir exists else make one
              if(!file_exists("uploads/")){
                mkdir("uploads/", 0755);
              }
              $newImageName = 'uploads/'.$id.".".$imageExt;
              //Upload the file to the dir
              if(move_uploaded_file($imageTmp, $newImageName)){
                //Add to the database

                //prepare the query
                $sql = "INSERT INTO products (id, name, price, description, image, quantity, available, date_event, date_added) VALUES ('$id', '$name', '$price', '$description', '$newImageName', '$quantity', '$quantity', '$Evtdate', '$TodayDate')";
                //Run the query
                $result = $DB->query($sql);
                if($result){
                  header('Location: ./create-product.php?s='.base64_encode("Your product is successfully added."));
                  die();
                }
                else{
                  //remove the image
                  unlink('uploads/'.$newImageName);
                }
              }
              else{
              header('Location: ./create-product.php?e='.base64_encode("An error has occured while uploading the file. The product is not added."));
              die();
              }
            }
            else{
              header('Location: ./create-product.php?e='.base64_encode("Your files must be images"));
              die();
            }
          }
          else{
            header('Location: ./create-product.php?e='.base64_encode("Name needs to be under the 50 characters"));
            die();
          }
        }
        else{
          header('Location: ./create-product.php?e='.base64_encode("quantity needs to be an number."));
          die();
        }
      }
      else{
        header('Location: ./create-product.php?e='.base64_encode("The price needs to be an number."));
        die();
      }
    }
    else{
      header('Location: ./create-product.php?e='.base64_encode("One or more fields are not filled in."));
      die();
    }
  }
  else{
    header('Location: ./create-product.php');
    die();
  }
?>
