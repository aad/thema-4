<?php require 'config.inc.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<?php require 'check.loggedin.php'; //Check if the user is logged in.?>
<div class="wrapper">
  <ul class="bread-crumbs">
    <li class="active">
      <i class="fa fa-shopping-cart"></i>
      Select Products
    </li>
    <!-- Add checklist -->
    <li class="active">
      <i class="fa fa-calculator"></i>
      Checkout
    </li>
    <li>
      <i class="fa fa-money"></i>
      Billing
    </li>
    <!-- Share -->
    <li>
      <i class="icon-credit-card"></i>
      Payment
    </li>

    <!-- Job complete -->
    <li>
      <i class="icon-ok-sign"></i>
      Order Complete
    </li>
  </ul>
<h2>Now In Your Cart:</h2>
  <div class="shopping-cart" id="shopping-cart">

    <div class="column-labels">
      <label class="product-image">Image</label>
      <label class="product-details">Product</label>
      <label class="product-price">Price</label>
      <label class="product-quantity">Quantity</label>
      <label class="product-removal">Remove</label>
      <label class="product-line-price">Total</label>
    </div>
    <?php
      $sql = "SELECT * FROM cart WHERE user_id='$user_id'";
      $result = $DB->query($sql);
      if($result->num_rows > 0):
        $sql = "SELECT cart.amount , products.price , products.name, products.image,
                       products.price, products.description, cart.product_id, cart.user_id
                FROM cart
                LEFT JOIN products
                ON cart.product_id=products.id WHERE cart.user_id='$user_id'";
        $result = $DB->query($sql);
        $totalCardPrice = 0;
        while($rows = $result->fetch_assoc()):
          $cartAmount = $rows['amount'];
          $cartPrice = $rows['price'];
          $cartImage = $rows['image'];
          $cartName = $rows['name'];
          $cartDesc = $rows['description'];
          $cartProductId = $rows['product_id'];
          $cartUserId = $rows['user_id'];

          $cartTotalPrice = $cartAmount*$cartPrice;
          $totalCardPrice  = $totalCardPrice+$cartTotalPrice;
    ?>
          <div class="product">
            <div style="display: none;">
              <input type="hidden" id="product_id" value="<?php print $cartProductId;?>">
              <input type="hidden" id="user_id" value="<?php print $cartUserId;?>">
            </div>
            <div class="product-image">
              <img src="<?php print $cartImage?>" width="100px;">
            </div>
            <div class="product-details">
              <div class="product-title"><?php print $cartName?></div>
              <p class="product-description"><?php print $cartDesc?></p>
            </div>
            <div class="product-price"><?php print $cartPrice?></div>
            <div class="product-quantity">
              <label><?php print $cartAmount?></label>
            </div>
            <div class="product-removal">
              <button class="remove-product">
                Remove
              </button>
            </div>
            <div class="product-line-price"><?php print $cartTotalPrice?></div>
          </div>
    <?php endwhile;?>
    <?php
      $shippingCost = 4;
      $totalInclVat = $totalCardPrice * (1 + 21 / 100.0);
      $grandTotal = $totalInclVat+$shippingCost;
      $totalVat = $totalInclVat-$totalCardPrice;
    ?>
    <div class="totals">
      <div class="totals-item">
        <label>Subtotal</label>
        <div class="totals-value" id="cart-subtotal"><?php print $CPC->setDecimals($totalCardPrice);?></div>
      </div>
      <div class="totals-item">
        <label>Tax (21%)</label>
        <div class="totals-value" id="cart-tax"><?php print $CPC->setDecimals($totalVat);?></div>
      </div>
      <div class="totals-item">
        <label>Transaction:</label>
        <div class="totals-value" id="cart-shipping"><?php print $shippingCost;?></div>
      </div>
      <div class="totals-item totals-item-total">
        <label>Grand Total</label>
        <div class="totals-value" id="cart-total"><?php print $CPC->setDecimals($grandTotal);?></div>
      </div>
    </div>
        <a href="tickets.php" class="btn btn-primary btn-lg">Shop More</a>
        <a class="checkout btn btn-success btn-lg" href="billing.php">Checkout</a>
      <?php else:?>
        <center>
          There are no products in your cart.<br />
          <a href="tickets.php">Click here to Shop</a>
        </center>
        <button class="checkout btn btn-success btn-lg" disabled="true">Checkout</button>
      <?php endif; ?>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
