<?php require 'check.admin.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<div class="wrapper">
  <?php

    if(!isset($_GET['id'])){
        header('Location: ./orders.php');
        die();
    }
    else{
      $order_id = strip_tags(htmlspecialchars($_GET['id']));
    }

    if(isset($_POST['deleteButton'])){
      $sql = "DELETE FROM orders WHERE order_id='$order_id'";
      $result = $DB->query($sql);
    }
    if(isset($_POST['changeStatusBtn'])){
      $newStatus = strip_tags(htmlspecialchars($_POST['status']));
      if($newStatus == '0' || $newStatus == '1' || $newStatus == '2' || $newStatus == '3' ){
          $sql = "UPDATE orders SET status='$newStatus' WHERE order_id='$order_id'";
          $DB->query($sql);
          $success = true;
      }
    }

    $sql = "SELECT orders.order_id, orders.status, orders.date, orders.quantity, products.name, orders.date
            FROM orders
            LEFT JOIN products ON orders.product_id = products.id WHERE orders.order_id='$order_id'";
    $result = $DB->query($sql);
    if(!$result){
      header('Location: ./orders.php');
    }

    $data = $result->fetch_assoc();
    $status = $data['status'];
  ?>
  <a href="orders.php" class="btn btn-primary">Back</a><br />
  <center>
    <h1>Order Status: <?php print $order_id;?></h1>
    <?php
      if($success == true){
        print '<div class="alert alert-success" role="alert">You have successfully changed the order status.</div><br />';
      }
    ?>
  <form action="change_order_status.php?id=<?php print $order_id;?>" method="post">
    <table>
      <tr>
        <td>
          Status:
        </td>
        <td>
          <select class="form-control" name="status">
            <?php
              switch ($status) {
                case 0:
                  print "<option value='0'>Pending Payment</option>";
                  break;
                case 1:
                  print "<option value='1'>Paid</option>";
                  break;
                case 2:
                print "<option value='2'>Send</option>";
                  break;
                case 3:
                print "<option value='3'>Canceled</option>";
                  break;
              }
            ?>
            <option value="none" disabled></option>
            <option value="0">Pending Payment</option>
            <option value="1">Paid</option>
            <option value="2">Send</option>
            <option value="3">Canceled</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>
          &nbsp;
        </td>
        <td>
          &nbsp;
        </td>
      </tr>
      <tr>
        <td>
          &nbsp;
        </td>
        <td>
          <input type="submit" name="changeStatusBtn" class="btn btn-success" value="Change">
        </td>
        <td>
          <input type="submit" name="deleteButton" class="btn btn-danger" value="Delete">
        </td>
      </tr>
    </table>
  </form>
</center>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
