/* Set rates + misc */
var taxRate = 21;
var shippingRate = 15.00;
var fadeTime = 200;


/* Assign actions */

$('.product-removal button').click( function() {
  removeItem(this);
});


/* Recalculate cart */
function recalculateCart()
{
  /*var subtotal = 0;

  //Sum up row totals
  $('.product').each(function () {
    subtotal = parseFloat($(this).children('.product-line-price').text());
  });

  // Calculate totals
  console.log(subtotal);
  var tax = subtotal * (1 + taxRate / 100.0);
  tax = tax - subtotal;
  console.log(tax);
  var shipping = (subtotal > 0 ? shippingRate : 0);
  var total = subtotal + tax + shipping;

  // Update totals display
  $('.totals-value').fadeOut(fadeTime, function() {
    $('#cart-subtotal').html(subtotal.toFixed(2));
    $('#cart-tax').html(tax.toFixed(2));
    $('#cart-shipping').html(shipping.toFixed(2));
    $('#cart-total').html(total.toFixed(2));
    if(total == 0){
      $('.checkout').fadeOut(fadeTime);
    }else{
      $('.checkout').fadeIn(fadeTime);
    }
    $('.totals-value').fadeIn(fadeTime);
  });*/
  setTimeout(function () {
        location.reload();
        //$('#shopping-cart').load(document.URL +  ' #shopping-cart');
    }, 1000);
}


/* Remove item from cart */
function removeItem(removeButton)
{
  var data = "user_id="+$("#user_id").val()+"&product_id="+$("#product_id").val();
  $.ajax({
    type: "POST",
    url:  "remove_from_cart.php",
    data: data,
    success: function (response) {
      /* Remove row from DOM and recalc cart total */
      var productRow = $(removeButton).parent().parent();
      productRow.slideUp(fadeTime, function() {
        productRow.remove();
        recalculateCart();
      });
    }
  });
}
