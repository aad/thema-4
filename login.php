<?php require 'config.inc.php'; //Include the config?>
<?php $loginPage = true; ?>
<?php require 'header.page.php'; //Include the page header?>
<div class="wrapper">
    <div class="loginForm">
      <h1>Login</h1>
      <?php if(isset($_GET['e'])):?>
        <div class="alert alert-danger" role="alert"><?php print base64_decode($_GET['e']);?></div>
      <?php endif;?>
      <form action="authenticate.php" method="post">
        <?php if(isset($_GET['checkout'])):?>
          <input type="hidden" name="checkout">
        <?php endif; ?>
        <table>
          <tr>
            <td>Email:</td>
            <td><input type="email" name="email" placeholder="Email" required></td>
          </tr>
          <tr>
            <td>Password:&nbsp;&nbsp;&nbsp;</td>
            <td><input type="password" name="password" placeholder="Password" required></td>
          </tr>
          <tr>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;</td>
          </tr>
          <tr>
            <td><a href="register.php">Register</a></td>
            <td><input type="submit" name="submitBtn" class="btn btn-primary" value="Login"></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
