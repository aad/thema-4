<?php require 'config.inc.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<?php

  //print "<pre>";
  //print_r($_SESSION['shopping_card']);
  //print "</pre>";

?>
<div class="wrapper">
  <?php if(!isset($_GET['q'])):?>
    <div class="order-container">
      <label>
        Now:
        <?php
        if(isset($_GET['order'])){
          $order = strip_tags(htmlspecialchars($_GET['order']));
          if($order == "date-old-new"){
            print "Old - New";
          }
          else if($order == "date-new-old"){
            print "New - Old";
          }
          else if($order == "price-low-exp"){
            print "Cheap - Expensive";
          }
          else if($order == "price-exp-low"){
            print "Expensive - Cheap";
          }
          else{
            print "New - Old";
          }
        }
        else{
            print "New - Old";
        }
        ?>
      | Order by:</label>
      <a href="tickets.php?order=date-old-new" class="btn btn-primary btn-outline">Old - New</a>
      <a href="tickets.php?order=date-new-old" class="btn btn-primary btn-outline">New - Old</a>
      <a href="tickets.php?order=price-low-exp" class="btn btn-primary btn-outline">Cheap - Expensive</a>
      <a href="tickets.php?order=price-exp-low" class="btn btn-primary btn-outline">Expensive - Cheap</a>
    </div>
  <?php endif;?>
    <div class="container">
      <div class="row">
        <?php
          //Maak sql aan en voer uit
          if(isset($_GET['q'])){
            $search = strip_tags(htmlspecialchars($_GET['q']));
            $sql = "SELECT * FROM products WHERE name LIKE '%$search%' AND available > 0 ORDER BY date_added DESC";
          }
          else if(isset($_GET['order'])){
            $order = strip_tags(htmlspecialchars($_GET['order']));
            if($order == "date-old-new"){
              $sql = "SELECT * FROM products WHERE available > 0 ORDER BY date_added ASC";
            }
            else if($order == "date-new-old"){
              $sql = "SELECT * FROM products WHERE available > 0 ORDER BY date_added DESC";
            }
            else if($order == "price-low-exp"){
              $sql = "SELECT * FROM products WHERE available > 0 ORDER BY price ASC";
            }
            else if($order == "price-exp-low"){
              $sql = "SELECT * FROM products WHERE available > 0 ORDER BY price DESC";
            }
            else{
              $sql = "SELECT * FROM products WHERE available > 0 ORDER BY date_added DESC";
            }
          }
          else{
              $sql = "SELECT * FROM products WHERE available > 0 ORDER BY date_added DESC";
          }
          $result = $DB->query($sql);
          //Kijk of er resultaten uit de database terug komen anders print een alert uit.
          if($result->num_rows == 0){
            if(isset($_GET['q'])){
              print "<div class='alert alert-danger' role='alert'>There were now products found with a name like: <b>$search</b></div>";
              print "<a href='tickets.php' class='btn btn-primary btn-outline'>Show Available Tickets</a>";
            }
            else{
                print "<div class='alert alert-danger' role='alert'>There are no product available right now.</div>";
            }
            die();
          }
          //Begin de while loop
          while($rows = $result->fetch_assoc()):
            print "<td>";
            $productId = $rows['id'];
            $productImage = $rows['image'];
            $productName = $rows['name'];
            $productPrice = $rows['price'];
            $productDesc = $rows['description'];
            $productEvtDate = $rows['date_event'];
            $productEvtDate = str_replace("T"," ",$productEvtDate);

            $productAvailable = $rows['available'];
            if($i == 3){
              print '</div>';
              print '<div class="row">';
            }
            $i++;
        ?>
          <div class="col-sm-6 col-md-4">
            <a href="product.php?id=<?php print $productId;?>">
            <div class="thumbnail" style="height: 430px; width: 380px;">
              <img src="<?php print $productImage;?>" alt="<?php print $productName;?> " width="400px;">
              <div class="caption">
                <h3><?php print $productName;?></h3>
                </a>
                <p>Date: <?php print $productEvtDate; ?> | Available: <?php print $productAvailable; ?></p>
                <p><?php print $productDesc;?></p>
                <p><label style="font-size: 20px; position: absolute; bottom: 30px; left: 30px;"><b>&euro;<?php print $productPrice?></b></label></p>
                <form action="#" method="post" style="position: absolute; bottom: 30px; right: 5px;">
                  <input type="hidden" name="product_id" value="<?php print $productId;?>">
                  <input type="number" name="amount" value="1" style="width: 50px; height: 30px; border-radius: 10px; text-align: right; border: 1px solid black;" required>
                  <input type="submit" name="addToCard" class="btn btn-success btn-outline btn-sx" value="Add to Cart">
                </form>
              </div>
            </div>
          </div>
        <?php endwhile;?>
      </div>
    </div>

    <?php if(isset($_GET['q'])): ?>
      <center><a href='tickets.php' class='btn btn-primary btn-outline' style='width: 350px; height: 60px; padding: 20px;'>Show Available Tickets</a></center>
    <?php endif;?>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
