<?php require 'check.admin.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<div class="wrapper">
  <?php require 'sidebar.page.php'; //Include the admin sidebar?>
  <div class="right-admin-side">
  <div class="searchIdForm">
    <form action="orders.php" method="GET">
      Search Orders: <input type="text" name="q" placeholder="Search">
    </form>
  </div>
    <?php
    $result = $DB->query("SELECT * FROM orders");
    if($result->num_rows > 0){
    $sql = "SELECT orders.order_id, orders.status, orders.date, users.name, orders.date
            FROM users
            LEFT JOIN orders ON orders.order_user = users.id WHERE orders.order_user = users.id ORDER BY date DESC";
      $q = strip_tags(htmlspecialchars($_GET['q']));
      if(isset($q)){
        if(!empty($q)){
          $sql = "SELECT orders.order_id, orders.status, orders.date, users.name, orders.date
                  FROM users
                  LEFT JOIN orders ON orders.order_user = users.id WHERE orders.order_id LIKE '%$q%' OR users.name LIKE '%$q%' ORDER BY date DESC";
        }
      }
    }
      //$sql = "SELECT * FROM orders";
      $result = $DB->query($sql);
      if($result->num_rows > 0):
    ?>
    <table class="table">
      <thead>
        <tr>
          <th>Status</th>
          <th>Order ID</th>
          <th>By</th>
          <th>Date</th>
          <th></th>
        </tr>
      </thead>
        <tbody>
    <?php
        while($row = $result->fetch_assoc()):
          $status = $row['status'];
          //$productName = $row['name'];
          //$orderQuantity = $row['quantity'];
          $orderUser = $row['name'];
          $orderDate = $row['date'];
          $orderId = $row['order_id'];
        ?>
        <tr>
        <td>
          <?php
            switch ($status) {
              case 0:
                print "<span class='label label-primary'>Pending Payment</span>";
                break;
              case 1:
                print "<span class='label label-warning'>Paid</span>";
                break;
              case 2:
                print "<span class='label label-success'>Send</span>";
                break;
              case 3:
                print "<span class='label label-danger'>Canceled</span>";
                break;
            }
          ?>
        </td>
        <td><?php print $orderId; ?></td>
        <td><?php print $orderUser; ?></td>
        <td><?php print $orderDate; ?></td>
        <td><a href="change_order_status.php?id=<?php print $orderId;?>" class="btn btn-primary"><i class="fa fa-cog"></i></a></td>
        </tr>
        <?php
        endwhile;
        ?>
      </tbody>
    </table>
        <?php
      else:
        print "No orders yet.";
      endif;
    ?>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
