<?php require 'config.inc.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<?php require 'check.loggedin.php'; //Check if the user is logged in.?>
<div class="wrapper">
  <h1>My Orders:</h1>
  <hr />
  <?php
    $sql = "SELECT * FROM orders WHERE order_user='$user_id' AND status > 0 ORDER BY date DESC";
    $result = $DB->query($sql);
    if($result->num_rows > 0){
      while($row = $result->fetch_assoc()){
        $idOrder = $row['order_id'];
        $dateOrder = $row['date'];
        $priceOrder = $row['price'];
        print "$dateOrder | <a href='order-details.php?order=$idOrder'>Order: $idOrder</a><div style='float: right;'>&euro;$priceOrder</div>";
        print "<hr />";
      }
    }
    else{
      print "You have not ordered anything yet.";
    }
  ?>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
