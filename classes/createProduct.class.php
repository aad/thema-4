<?php
  /**
   * create product Class
   */
  class createProduct
  {
    //Make Unique key
    function makeId(){
      // With this precision (microsecond) ID will looks like '2di2adajgq6h'
      $id = base_convert(microtime(false), 10, 36);
      // With less precision (second) ID will looks like 'niu7pj'
      //$id = base_convert(time(), 10, 36);
      return $id;
    }

    //Comma Check for price
    function commaCheck($price){
      //Check which caracter is filled in. A dot or a comma
        if(strpos($price, '.')){
          //the sting contains a dot
          return $price;
        }
        else if(strpos($price, ',')){
          //the string contains a comma
          //replace the comma with a dot
          $priceWithDot = str_replace(",", ".", $price);
          return $priceWithDot;
        }
        else{
          return "This Are Characters";
        }
    }
    //Round decimals of price
    function setDecimals($price){
      //the sting contains a dot
      $newPrice =  number_format((float)$price, 2, '.', '');

      return $newPrice;
    }
  }

?>
