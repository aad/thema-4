<?php require 'config.inc.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<!--Full Width Slider-->
<?php
  $sql = "SELECT * FROM slider";
  $result = $DB->query($sql);
  if($result->num_rows > 0):
?>
<div class="Modern-Slider">
  <?php
    while($rows = $result->fetch_assoc()):
      $SLTitle = $rows['title'];
      $SLImage = $rows['image'];
      $SLDesc = $rows['description'];
      $SLLink = $rows['link'];
  ?>
  <!-- Item -->
  <div class="item">
    <a href="<?php print $SLLink;?>">
      <div class="img-fill">
        <img src="<?php print $SLImage;?>" alt="<?php print $SLTitle;?>">
        <div class="info">
          <div>
            <h3><?php print $SLTitle;?></h3>
            <h5><?php print $SLDesc;?></h5>
          </div>
        </div>
      </div>
    </a>
  </div>
  <!-- // Item -->
<?php endwhile; ?>
</div>
<?php endif; ?>
<?php
  $sql = "SELECT * FROM products LIMIT 5";
  $result = $DB->query($sql);
  if($result->num_rows > 0):
?>
<div class="highlight-tickets">
  <ul class="items">
    <?php
      while($rows = $result->fetch_assoc()):
        $productImage = $rows['image'];
        $productName = $rows['name'];
        $productId = $rows['id'];
    ?>
  	<li>
  		<div class="bg-img" style="background-image: url('<?php print $productImage; ?>');"></div>
  		<a href="product.php?id=<?php print $productId; ?>">
  			<div class="content">
  				<h2><?php print $productName; ?></h2>
  			</div>
  		</a>
  	</li>
    <?php endwhile; ?>
  </ul>
</div>
<?php endif; ?>
<div class="introScreen">
  <center><a href="tickets.php" class="btn btn-primary btn-outline btn-lg" style="width: 500px; height: 60px; padding: 20px;">Show Tickets</a></center>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
