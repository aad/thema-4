<?php require 'config.inc.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<?php require 'check.loggedin.php'; //Check if the user is logged in.?>
<div class="wrapper">
  <a href="my-orders.php" class="btn btn-primary">Back</a>
  <?php
    $order_id = strip_tags(htmlspecialchars($_GET['order']));
    $sql = "SELECT * FROM orders WHERE order_id='$order_id' AND order_user='$user_id' AND status='1'";
    $result = $DB->query($sql);
    if($result->num_rows < 1){
      header('Location: ./my-orders.php');
      die();
    }
    $fetchData = $result->fetch_assoc();
    $totalPrice = $fetchData['price'];
    $paymentMethod = $fetchData['payment_method'];
    ?>
    <h1>Order: <?php print $order_id;?> |
    paid with:
    <?php
      switch ($paymentMethod) {
        case 'credit_card':
          print "Credit Card";
          break;
      }
    ?>
    </h1>
    <div class="shopping-cart" id="shopping-cart">
      <div class="column-labels">
        <label class="product-image">Image</label>
        <label class="product-details">Product</label>
        <label class="product-price">Price</label>
        <label class="product-quantity">Quantity</label>
        <label class="product-line-price">Total</label>
      </div>
      <?php
        $sql = "SELECT * FROM order_items WHERE order_id='$order_id'";
        $result = $DB->query($sql);
        if($result->num_rows > 0):
          $sql = "SELECT order_items.quantity , order_items.price , products.name, products.image,
                          products.description
                  FROM order_items
                  LEFT JOIN products
                  ON order_items.product_id=products.id WHERE order_items.order_id='$order_id'";
          $result = $DB->query($sql);
          $totalCardPrice = 0;
          while($rows = $result->fetch_assoc()):
            $cartAmount = $rows['quantity'];
            $cartPrice = $rows['price'];
            $cartImage = $rows['image'];
            $cartName = $rows['name'];
            $cartDesc = $rows['description'];
            $cartProductId = $rows['product_id'];
            $cartUserId = $rows['user_id'];

            $cartTotalPrice = $cartAmount*$cartPrice;
            $totalCardPrice  = $totalCardPrice+$cartTotalPrice;
      ?>
      <div class="product">
        <div class="product-image">
          <img src="<?php print $cartImage?>" width="100px;">
        </div>
        <div class="product-details">
          <div class="product-title"><?php print $cartName?></div>
          <p class="product-description"><?php print $cartDesc?></p>
        </div>
        <div class="product-price"><?php print $cartPrice?></div>
        <div class="product-quantity">
          <label><?php print $cartAmount?></label>
        </div>
        <div class="product-line-price"><?php print $cartTotalPrice?></div>
      </div>

      <?php endwhile;?>
      <?php
        $shippingCost = 15;
        $totalInclVat = $totalCardPrice * (1 + 21 / 100.0);
        $grandTotal = $totalInclVat+$shippingCost;
        $totalVat = $totalInclVat-$totalCardPrice;

        $CPC = new createProduct();
      ?>
      <div class="totals">
        <div class="totals-item">
          <label>Subtotal</label>
          <div class="totals-value" id="cart-subtotal"><?php print $CPC->setDecimals($totalCardPrice);?></div>
        </div>
        <div class="totals-item">
          <label>Tax (21%)</label>
          <div class="totals-value" id="cart-tax"><?php print $CPC->setDecimals($totalVat);?></div>
        </div>
        <div class="totals-item">
          <label>Shipping</label>
          <div class="totals-value" id="cart-shipping"><?php print $shippingCost;?></div>
        </div>
        <div class="totals-item totals-item-total">
          <label>Grand Total</label>
          <div class="totals-value" id="cart-total"><?php print $CPC->setDecimals($grandTotal);?></div>
        </div>
      </div>
        <?php else:?>
          <center>
            An error has occured.
          </center>
        <?php endif; ?>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
