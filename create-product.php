<?php require 'check.admin.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<div class="wrapper">
  <?php require 'sidebar.page.php'; //Include the admin sidebar?>
  <div class="right-admin-side">
    <a href="products.php" class="btn btn-primary">Terug</a><br />
    <form action="makeProduct.php" id="create-product-form" method="POST" enctype="multipart/form-data">
    <?php if(isset($_GET['e'])):?>
      <div class="alert alert-danger" role="alert"><?php print base64_decode(strip_tags($_GET['e']));?></div>
    <?php endif; ?>
    <?php if(isset($_GET['s'])):?>
      <div class="alert alert-success" role="alert"><?php print base64_decode(strip_tags($_GET['s']));?></div>
    <?php endif; ?>
    <div class="form-group">
      <label for="name">Product Name <span>Max x Characters</span></label>
      <input type="text" name="name" id="name" required="required" class="form-controll"/>
    </div>

    <div class="form-group">
      <label for="date">Date and Time</label>
      <input type="datetime-local" name="date" id="date" required="required" class="form-controll"/>
    </div>

    <div class="form-group">
      <label for="price">Price</label>
      <input type="text" name="price" id="price" required="required" class="form-controll"/>
    </div>

    <div class="form-group">
      <label for="price">Quantity</label>
      <input type="number" name="quantity" id="quantity" required="required" class="form-controll"/>
    </div>

    <div class="form-group">
      <label for="description">Description</label>
      <textarea name="description" id="description" class="form-controll" required="required" rows="8" cols="40"></textarea>
    </div>

    <div class="form-group file-area">
          <label for="images">Images <span>Your images should be at least 400x300 wide</span></label>
      <input type="file" name="images" id="images" required="required" />
      <div class="file-dummy">
        <div class="success">Great, your files are selected. Keep on.</div>
        <div class="default">Please select some files</div>
      </div>
    </div>

    <div class="form-group">
      <button type="submit" name="submitBtn" class="btn btn-primary btn-outline">Create Product</button>
    </div>

  </form>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
