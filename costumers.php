<?php require 'check.admin.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<div class="wrapper">
  <?php require 'sidebar.page.php'; //Include the admin sidebar?>
  <div class="right-admin-side">
    <table class="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Rank</th>
          </tr>
        </thead>
          <tbody>
          <?php
            $sql = "SELECT * FROM users";
            $result = $DB->query($sql);
            if($result->num_rows > 0){
            while($row = $result->fetch_assoc()):
              $DBusername = $row['name'];
              $DBemail = $row['email'];
              $DBrank = $row['rank'];
          ?>
          <tr>
            <td><?php print $DBusername; ?></td>
            <td><?php print $DBemail; ?></td>
            <td>
              <?php
                switch ($DBrank) {
                  case 0:
                    print "<span class='label label-primary'>Normal User</span>";
                    break;
                  case 1:
                    print "<span class='label label-warning'>Super User</span>";
                    break;
                  case 2:
                    print "<span class='label label-success'>Admin</span>";
                    break;
                }
              ?>
            </td>
          </tr>
        <?php endwhile; ?>
        <?php
          }
          else{
            print "There are no users registered.";
          }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
