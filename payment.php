<?php require 'config.inc.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<?php require 'check.loggedin.php'; //Check if the user is logged in.?>
<div class="wrapper">
  <ul class="bread-crumbs">
    <li class="active">
      <i class="fa fa-shopping-cart"></i>
      Select Products
    </li>
    <!-- Add checklist -->
    <li class="active">
      <i class="fa fa-calculator"></i>
      Checkout
    </li>
    <li class="active">
      <i class="fa fa-money"></i>
      Billing
    </li>
    <!-- Share -->
    <li class="active">
      <i class="icon-credit-card"></i>
      Payment
    </li>

    <!-- Job complete -->
    <li>
      <i class="icon-ok-sign"></i>
      Order Complete
    </li>
  </ul>

<?php
require 'vendor/autoload.php';

$order_id = strip_tags(htmlspecialchars($_GET['order']));
$sql = "SELECT * FROM orders WHERE order_id='$order_id' AND order_user='$user_id'";
$result = $DB->query($sql);

if($result->num_rows < 1){
  header('Location: ./cart.php');
  die();
}
$dataFetch = $result->fetch_assoc();

$amount = $dataFetch['price'];
$orderIdd = $dataFetch['order_id'];
if (strpos($amount, ".")) {
  //There is a dot in the price
  $arr = explode(".", $amount, 2);
  $euros = $arr[0];
  $cents1 = $arr[1];

  $cents = $euros*100;
  $cents = $cents+$cents1;
}

$payMentMethod = $dataFetch['payment_method'];
if($payMentMethod == 'credit_card'){
  include 'pages/credit-card-payment.inc.php';
}
else if($payMentMethod == 'bitcoin'){
  include 'pages/bitcoin-payment.inc.php';
}
?>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
