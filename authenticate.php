<?php
  //Check if submit button is posted
  //1
  if(isset($_POST['submitBtn'])){
    //Set post values into variables
    $email = strip_tags(htmlspecialchars($_POST['email']));
    $password = sha1(strip_tags(htmlspecialchars($_POST['password'])));
    //Check if they are both filled in
    //2
    if (isset($email) && isset($password)) {
      //Require Database
      require 'db.inc.php';
      //Create sql
      $sql = "SELECT * FROM users WHERE email='$email' AND password='$password'";
      //Run query
      $result = $DB->query($sql);
      //Check if there are more than 0 results found
      //3
      if($result->num_rows > 0){
        //There is an user found.

        //Date time
        $date = date('Y-m-d G:i:s');

        //Start session
        session_start();
        $_SESSION['login_session'] = sha1(rand(40, 999).$date.$email.rand(10,500));
        $_SESSION['email'] = $email;
        //Update database with session id
        $sessionId = $_SESSION['login_session'];
        $DB->query("UPDATE users SET session_id='$sessionId' WHERE email='$email'");
        //Check if user is admin
        $row = $result->fetch_assoc();
        $rank = $row['rank'];
        if($rank == 2){
          //User is admin
          $_SESSION['admin_session'] = sha1(rand(20, 999).$email.rand(4,900));
          $adminId = $_SESSION['admin_session'];
          $DB->query("UPDATE users SET admin_id='$adminId' WHERE email='$email'");
        }

        //Header naar tickets
        header('Location: ./tickets.php');
      }
      else{//3
        header('Location: ./login.php?e='.base64_encode("Incorrect username and/or password."));
        die();
      }
    }
    else{//2
      header('Location: ./login.php?e='.base64_encode("You need to fill in your username and/or password."));
      die();
    }
  }
  else{//1
    header('Location: ./login.php');
    die();
  }
?>
