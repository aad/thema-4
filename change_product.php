<?php require 'check.admin.php'; //Include the config?>
<?php require 'header.page.php'; //Include the page header?>
<div class="wrapper">
  <?php require 'sidebar.page.php'; //Include the admin sidebar?>
  <div class="right-admin-side">
    <?php if (isset($error)): ?>
      <div class="alert alert-danger">
        <?php print $error; ?>
      </div>
    <?php endif; ?>
    <?php
      $productId = strip_tags(htmlspecialchars($_GET['id']));
      $sql = "SELECT * FROM products WHERE id='$productId'";
      $result = $DB->query($sql);
      if($result->num_rows > 0):

        $row = $result->fetch_assoc();
        $PRid = $row['id'];
        $productName = $row['name'];
        $productPrice = $row['price'];
        $productQuantity = $row['quantity'];
        $productsAvailable = $row['available'];
    ?>
    <?php
      if(isset($_GET['deleteConfirm'])){
        //Delete button is pressed.
        $sql = "DELETE FROM products WHERE id='$PRid'";
        $result = $DB->query($sql);
        if($result){
          header('Location: ./products.php');
        }
        else{
          $error = "An error has occured while removing this product from the database.";
        }
      }
      if (isset($_POST['submitButton'])) {
        $productName        = strip_tags(htmlspecialchars($_POST['productName']));
        $productPrice       = strip_tags(htmlspecialchars($_POST['price']));
        $productQuantity    = strip_tags(htmlspecialchars($_POST['quantity']));
        $productsAvailable  = strip_tags(htmlspecialchars($_POST['available']));

        if($productName && $productPrice && $productQuantity && $productsAvailable){
          if (!empty($productName) && !empty($productPrice) && !empty($productQuantity) && !empty($productsAvailable)) {
            //All fields are filled in.
            //Check if the fields are numeric
            if(is_numeric($productPrice) && is_numeric($productQuantity) && is_numeric($productsAvailable)){
              //All fields are filled in correctly.
              $sql = "UPDATE products SET name='$productName', price='$productPrice', quantity='$productQuantity', available='$productsAvailable' WHERE id='$PRid'";
              //Voer de query uit
              $result = $DB->query($sql);
              if($result){
                $success = "You have successfully changed your product.";
              }
              else{
                $error = "An error has occured while changing the product.";
              }
            }
            else{
              $error = "Price, Quantity and Available all need to be numbers.";
            }
          }
          else{
            $error = "All fields need to have values.";
          }
        }
        else{
          $error = "Not all fields are filled in.";
        }
      }
    ?>
    <?php if (isset($success)): ?>
      <div class="alert alert-success">
        <?php print $success; ?>
      </div>
    <?php endif; ?>
  <form action="#" method="POST">
    <table>
      <tr>
        <td>
          Product Name:
        </td>
        <td>
          <input type="text" name="productName" value="<?php print $productName; ?>">
        </td>
      </tr>

      <tr>
        <td>
          Product Price:
        </td>
        <td>
          <input type="text" name="price" value="<?php print $productPrice; ?>">
        </td>
      </tr>

      <tr>
        <td>
          Product Quantity:
        </td>
        <td>
          <input type="number" name="quantity" value="<?php print $productQuantity; ?>">
        </td>
      </tr>

      <tr>
        <td>
          Product Available:
        </td>
        <td>
          <input type="number" name="available" value="<?php print $productsAvailable; ?>">
        </td>
      </tr>
      <tr>
        <td>
          &nbsp;
        </td>
        <td>

        </td>
      </tr>
      <tr>
        <td>
          <input type="button" class="btn btn-danger" onclick="deleteProduct();" value="Delete Product" />
        </td>
        <td>
          <input type="submit" name="submitButton" class="btn btn-primary" value="Change Product" />
        </td>
      </tr>
    </table>
    <script type="text/javascript">
      function deleteProduct(){
        var conf = confirm("Are you sure you want to delete this product?");

        if(conf == true){
          window.location.href = "change_product.php?id=<?php print $PRid;?>&deleteConfirm";
        }
      }
    </script>
  </form>
        <?php
      else:
        print "No products yet.";
      endif;
    ?>
  </div>
</div>
<?php require 'footer.page.php'; //Include the page footer?>
